<?php 
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

$site_url = str_replace('wb-helper-app/', '', $_SERVER['REQUEST_URI']);
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>WB Helper</title>
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script> -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link rel="stylesheet" href="css/style.css">
  <script src="js/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
  <script src="js/common.js"></script>
  <script src="js/autosize.min.js"></script>
</head>
<body>
  <div class="main">
    <ul id="tabs">
      <li><a data-tab="form-normalizer" class="active">Нормализатор форм</a></li>
      <li><a data-tab="thumb-gallery-generator">Нормализатор галерей</a></li>
      <li class="link"><a href="<?php echo $site_url; ?>" target="_blank">Перейти на сайт <i class="fa fa-external-link"></i></a></li>
      <li class="link"><a href="https://docs.google.com/document/d/1BCDlL6qYomhLdfVg0TaTFEBI_cfMoWh9G_K0OgWQWmk/edit?usp=sharing" target="_blank">Инструкция <i class="fa fa-external-link"></i></a></li>
    </ul>
    <div id="tabs-content">
      <div data-tab-content="form-normalizer" class="active">
        <h2>Нормализатор форм WB</h2>
        <table class="form">
          <tbody>
            <tr>
              <td>
                <span class="subtitle">Путь к файлам сообщения об успехе <i>построчно</i></span>
                <textarea spellcheck="false" name="file_path_success" placeholder="./success.php" required="required"></textarea>
              </td>
              <td>
                <span class="subtitle">Путь к файлам сообщения об ошибке <i>построчно</i></span>
                <textarea spellcheck="false" name="file_path_error" placeholder="./error.php"></textarea>
              </td>
            </tr>
            <tr>
              <td>
                <span class="subtitle">Список id форм<sup>*</sup> и id кнопок для popup через ":" <i>построчно</i><br> например: Form1:Btn1</span>
                <textarea spellcheck="false" name="form_id" placeholder="Form1" required="required"></textarea>
              </td>
              <td>
                <span class="subtitle">Путь к файлам всплывающих форм <i>построчно</i></span>
                <textarea spellcheck="false" name="file_path_popup" placeholder="./form_popup.php"></textarea>
              </td>
            </tr>
            <tr>
              <td>
                <span class="subtitle">Почты для уведомлений<sup>*</sup> <i>через запятую</i></span>
                <input type="text" name="msg_email" placeholder="mymail@gmail.com" required="required">
              </td>
              <td>
                <span class="subtitle">Темы уведомлений<sup>*</sup> <i>построчно</i></span>
                <textarea spellcheck="false" type="text" name="msg_subject" placeholder="Заявка с формы в шапке" required="required"></textarea>
              </td>
            </tr>
            <tr class="result hide">
              <td colspan="2">
                <div class="result"></div>
              </td>
            </tr>
            <tr>
              <td colspan="2">
                <button type="submit" id="normalizeForms" class="btn btn-primary btn-block btn-large">Сгенерировать формы & Сохранить настройки</button>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div data-tab-content="thumb-gallery-generator">
        <table class="gallery-generator form col-2">
          <tbody>
            <tr>
              <td><h2>Генератор превью галерей fancybox WB</h2></td>
            </tr>
            <tr>
              <td>
                <span class="subtitle">Автоматически генерировать?</span>
                <label><input class="autogen" type="radio" name="autogen" value="1"> <span>Да</span></label>
                <label><input class="autogen" type="radio" name="autogen" value="0"> <span>Нет</span></label>
              </td>
            </tr>
            <tr>
              <td>
                <button type="submit" id="generateGalleryThumbs" class="btn btn-primary btn-block btn-large">Сгенерировать превью & Сохранить настройки</button>
              </td>
            </tr>
          </tbody>
        </table>

        <table class="gallery-combiner form col-2">
          <tbody>
            <tr class="heading">
              <td><h2>Объединитель галерей fancybox WB</h2></td>
            </tr>
            <tr>
              <td class="control-btn">
                <button class="btn btn-primary btn-block btn-small">Добавить галерею</button>
              </td>
            </tr>
            <tr>
              <td>
                <button type="submit" id="combineForms" class="btn btn-primary btn-block btn-large">Объединить галереи & Сохранить настройки</button>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</body>








