jQuery(document).ready(function($) {

  (function(){
    var bFormHelper = $('#normalizeForms');
    var bThumbGalleryGenerator = $('#generateGalleryThumbs');
    var bCombineForms = $('#combineForms');

    // Thumbs Gallery Generator, generate & save config
    bThumbGalleryGenerator.click(function(e) {
      e.preventDefault();

      var self = $(this),
          form = self.parents('.form'),
          isAutoGen = form.find('input[name="autogen"]:checked').val();

      $.ajax({
        url: './main_handler.php',
        type: 'post',
        dataType: 'json',
        data: {
          type:      'thumb_gallery_generator',
          isAutoGen: isAutoGen
        },
      })
      .done(function(json) {
        console.log("Success! Thumbs Gallery Generator.");
        console.log(json);
        bThumbGalleryGenerator.before('<div class="result"><span class="success">Настройки сохранены. Превью сгенерированы.</span></div>');
      })
      .fail(function() {
        console.log("Error! Thumbs Gallery Generator.");
      })
      .always(function() {
        console.log("Complete! Thumbs Gallery Generator.");
      });
    });

    // Form Normalizer, generate & save config
    bFormHelper.click(function(e) {
      e.preventDefault();

      var self = $(this),
          valid = true,
          form = self.parents('.form'),
          trResult = form.find('tr.result'),
          msgResult = trResult.find('.result'),
          filePathSuccess = form.find('[name="file_path_success"]'),
          filePathError = form.find('[name="file_path_error"]'),
          filePathPopup = form.find('[name="file_path_popup"]'),
          filePathSuccessVal = (filePathSuccess.val().length) ? filePathSuccess.val() : '';
          filePathErrorVal = (filePathError.val().length) ? filePathError.val() : '';
          filePathPopupVal = (filePathPopup.val().length) ? filePathPopup.val() : '';
          formIds = form.find('[name="form_id"]'),
          msgEmail = form.find('[name="msg_email"]'),
          msgSubject = form.find('[name="msg_subject"]'),
          fieldsArrImportant = [formIds, msgEmail, msgSubject],
          valid = validateField(fieldsArrImportant),
          // Для сохранения высоты textarea
          visualFormCss = {
            'file_path_success': filePathSuccess.outerHeight(), 
            'file_path_error':   filePathError.outerHeight(), 
            'file_path_popup':   filePathPopup.outerHeight(), 
            'form_id':           formIds.outerHeight(), 
            'msg_subject':       msgSubject.outerHeight()
          };

      console.log(visualFormCss);

      trResult.addClass('hide');
      msgResult.html('');

      if (!valid) {
        trResult.removeClass('hide');
        msgResult.append('<span class="error">Поля не должны быть пустыми!</span>');
        return false;
      } else {
        trResult.removeClass('hide');
        msgResult.append('<span class="success">Форма заполнена корректно. Данные сгенерированы.</span>');
      }

      // Generate files & save settings
      $.ajax({
        url: './main_handler.php',
        type: 'post',
        dataType: 'json',
        data: {
          type:              'form_normalizer',
          filePathSuccess:   filePathSuccessVal,
          filePathError:     filePathErrorVal,
          filePathPopup:     filePathPopupVal,
          formIds:           formIds.val(),
          msgEmail:          msgEmail.val(),
          msgSubject:        msgSubject.val(),
          visualFormCss:     visualFormCss
        },
      })
      .done(function(json) {
        console.log("success");
        console.log(json);
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });
    });

    // Combine Forms btn, generate & save config
    bCombineForms.click(function(e) {
      e.preventDefault();

      var self = $(this);
      var ids = $('textarea[name="combine_gallery_id[]"]').map(function(){return $(this).val();}).get();

      $.ajax({
        url: './main_handler.php',
        type: 'post',
        dataType: 'json',
        data: {
          type: 'gallery_combiner',
          ids:  ids
        },
      })
      .done(function(json) {
        console.log("Success! Gallery Combiner.");
        console.log(json);
        bCombineForms.before('<div class="result"><span class="success">Настройки сохранены. Галереи объединены.</span></div>');
      })
      .fail(function() {
        console.log("Error! Gallery Combiner.");
      })
      .always(function() {
        console.log("Complete! Gallery Combiner.");
      });
    });
  })();

  // Get settings Form Normalizer
  (function(){
    var f = $('[data-tab-content="form-normalizer"] table.form'),
        filePathSuccess = f.find('[name="file_path_success"]'),
        filePathError = f.find('[name="file_path_error"]'),
        filePathPopup = f.find('[name="file_path_popup"]'),
        formIds = f.find('[name="form_id"]'),
        msgEmail = f.find('[name="msg_email"]'),
        msgSubject = f.find('[name="msg_subject"]');

    $.ajax({
      url: './form-normalizer/config.php',
      type: 'post',
      dataType: 'json',
      data: {},
    })
    .done(function(json) {
      console.log("Get settings success! Form Normalizer.");
      console.log(json);
      if (json.success_files.length) {
        filePathSuccess.val(json.success_files.join('\n'));
        filePathSuccess.attr('style', 'height:'+json.visual_form_css.file_path_success+'px;');
      }
      if (json.error_files.length) {
        filePathError.val(json.error_files.join('\n'));
        filePathError.attr('style', 'height:'+json.visual_form_css.file_path_error+'px;');
      }
      if (json.popup_files.length) {
        filePathPopup.val(json.popup_files.join('\n'));
        filePathPopup.attr('style', 'height:'+json.visual_form_css.file_path_popup+'px;');
      }
      formIds.val(json.form_ids.join('\n'));
      formIds.attr('style', 'height:'+json.visual_form_css.form_id+'px;');
      msgEmail.val(json.to_email);
      msgSubject.val(json.msg_subjects.join('\n'));
      msgSubject.attr('style', 'height:'+json.visual_form_css.msg_subject+'px;');
    })
    .fail(function() {
      console.log("Get settings error! Form Normalizer.");
    })
    .always(function() {});
  })();


  // Get settings Thumbs Gallery Generator
  (function(){
    var f = $('[data-tab-content="thumb-gallery-generator"] table.gallery-generator');

    $.ajax({
      url: './thumb-gallery-generator/config.php',
      type: 'post',
      dataType: 'json',
      data: {},
    })
    .done(function(json) {
      console.log("Get settings success! Thumbs Gallery Generator.");
      console.log(json);

      f.find('input[name="autogen"][value="'+ json.is_autogen +'"]').prop( "checked", true );
    })
    .fail(function() {
      console.log("Get settings error! Thumbs Gallery Generator.");
    })
    .always(function() {});
  })();

  // Get settings Gallery Combiner
  (function(){
    var f = $('[data-tab-content="thumb-gallery-generator"] table.gallery-combiner');
    var head = f.find('tr.heading');

    $.ajax({
      url: './gallery-combiner/config.php',
      type: 'post',
      dataType: 'json',
      data: {},
    })
    .done(function(json) {
      var l = json.formsId.length;
      if (l) {
        for (var i = l - 1; i >= 0; i--) {
          if (json.formsId[i].oldId.length) {
            var val = json.formsId[i].oldId.join('\n');
            var htmlTr = '<tr> <td> <span class="subtitle">Id галерей для объединения <i>построчно</i> <i class="remove">X</i> <i class="right">Галерея #<b></b></i></span> <textarea spellcheck="false" name="combine_gallery_id[]" placeholder="Gallery1">'+val+'</textarea> </td> </tr>';
            head.after(htmlTr);
          }
        }
      }
      console.log("Get settings success! Gallery Combiner.");
    })
    .fail(function() {
      console.log("Get settings error! Gallery Combiner.");
    })
    .always(function() {});
  })();

  // Menu tabs
  (function(){
    var tabsMenu = $('#tabs'),
        tabsContent = $('#tabs-content'),
        tabs = tabsMenu.find('a[data-tab]'),
        content = tabsContent.find('div[data-tab-content]');

        tabs.on('click', content, function(e) {
          e.preventDefault();
          var el = $(this),
              elData = el.attr('data-tab'),
              elContent = tabsContent.find('div[data-tab-content="'+elData+'"]');

          tabs.removeClass('active');
          content.removeClass('active');

          el.addClass('active');
          elContent.addClass('active');
          
        });
  })();

  // Gallery Combiner add new tr
  (function(){
    var addBtn = $('td.control-btn button');
    var fieldsNum = $('textarea[name="combine_gallery_id[]"]').length;

    addBtn.click(function(e) {
      var tr = $(this).parents('tr');
      fieldsNum++;
      tr.before('<tr> <td> <span class="subtitle">Id галерей для объединения <i>построчно</i> <i class="remove">X</i> <i class="right">Галерея #<b></b></i></span> <textarea spellcheck="false" name="combine_gallery_id[]" placeholder="Gallery1" style="overflow: hidden; word-wrap: break-word; resize: none; height: 60px;"></textarea> </td> </tr>');
    });

    $(document).on('click', 'i.remove', function(e) {
      e.preventDefault();
      var trField = $(this).parents('tr');
      trField.remove();
      fieldsNum--;
    });
  })();

  function validateField(elements) {
    var valid = true;

    for (var i = elements.length - 1; i >= 0; i--) {
      $(elements[i]).removeClass('error');
      
      if ($(elements[i]).val().trim().length === 0) {
        $(elements[i]).addClass('error');
        valid = false;
      }

    }

    return valid;
  }

  $(document).bind('ready click', function(){
    autosize($('textarea'));
  });

});

















