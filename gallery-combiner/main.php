<?php 
  class GalleryCombiner {

    private $json = array();
    private $galleries_combined = 0;
    private $formsId = array();
    private $website_pages = array();

    public function start_helper() {
      // Собираем данные запроса
      if (isset($_POST['ids'])) {
        $ids = $_POST['ids']; $k = 0;
        foreach ($ids as $key => $value) {
          $this->formsId[$k]['oldId'] = $this->string_to_array($value);
          $this->formsId[$k]['newId'] = 'wbFancyboxGalleru'.$k;
          $k++;
        }
      }

      // Сохраняем настройки
      $config_fields = array(
        'formsId' => $this->formsId
      );

      $this->save_config('gallery-combiner/config.php', $config_fields);

      $p = str_replace('wb-helper-app/gallery-combiner', '', realpath(dirname(__FILE__)));
      $dir_iterator = new RecursiveDirectoryIterator($p);
      $iterator = new RecursiveIteratorIterator($dir_iterator, RecursiveIteratorIterator::SELF_FIRST);

      foreach ($iterator as $path) {
        if ($path->isFile()) {
          $file = pathinfo($path);
          $name = $file['basename']; $ext = $file['extension']; $dir = $file['dirname'];
          if (($ext == 'php' || $ext == 'html' || $ext == 'htm') && strpos($dir, 'wb-helper') == false) {
            $this->website_pages[] = $dir.'/'.$name;
          }
        }
      }

      $regex = '/(data-rel=[\'|"]|attr\(\'rel\', \'|rel\^=[\'|"])(.*?)([\'|"])/';
      $p_c = count($this->website_pages);
      for ($i=0; $i < $p_c; $i++) {
        $content = file_get_contents($this->website_pages[$i]);
        $content = preg_replace_callback(
          $regex, 
          function ($m) {
            $f_c = count($this->formsId);
            for ($i=0; $i < $f_c; $i++) {
              foreach ($this->formsId[$i]['oldId'] as $old_id) {
                if ($m[2] == $old_id) {
                  $m[2] = $this->formsId[$i]['newId'];
                  $this->galleries_combined++;
                }
              }
            }
            return $m[1].$m[2].$m[3];
          }, 
          $content
        );

        file_put_contents($this->website_pages[$i], $content);
      }

      // json messages
      $this->json['replacements'] = $this->galleries_combined;
      return $this->json;
    }

    protected function get_config($filename) {
      if (file_exists($filename)) {
        $info = file_get_contents($filename);
        $info = json_decode($info, TRUE);
        return $info;
      }
    }

    protected function save_config($filename, array $config, $filetype = 'php') {
      if ($filetype == 'php') {
        $config = var_export($config, true);
        file_put_contents($filename, "<?php echo json_encode($config);");
      } elseif ($filetype == 'json') {
        file_put_contents($filename, json_encode($config));
      }
    }

    protected function string_to_array($string, $replace = '/\r|\n/') {
      $result = false;
      if ($string) {
        $result = preg_replace($replace, ',', $string);
        $result = explode(',', $result);
      }
      return $result;
    }

    protected function debug($title, $var, $clear = false) {
      $file = '/home/d/dbyria29/dbyria29.bget.ru/public_html/wb9/log.txt';

      $var = !is_array($var) ? $var : var_export($var, true);

      $result  = "\n--------------------\n";
      $result .= date("Y-m-d H:i:s")."\n".$title."\n".$var;

      if ($clear) { file_put_contents($file, ''); }

      file_put_contents($file, $result, FILE_APPEND);
    }

  }
?>