<?php 
  class FormNormalizer {
    
    private $json = array();
    private $files_copied = 0;
    private $success_files = array();
    private $error_files = array();
    private $popup_files = array();
    private $form_ids = array();
    private $popup_btns_id = array();
    private $to_email = '';
    private $msg_subjects = array();
    private $forms_num = 0;
    private $forms_and_msgs_css_file = '';
    private $visualFormCss = '';
    private $form_ids_c = 0;
    private $success_files_c = 0;
    private $popup_files_c = 0;
    private $count_success_media = 0;
    private $count_success_no_media = 0;
    private $count_popup_media = 0;
    private $count_popup_no_media = 0;
    private $website_pages = array();

    private $height_arr = array();
    private $height_no_media_arr = array();

    public function start_helper() {
      // Подключаем парсер 
      include('parser/simple_html_dom.php');

      // Собираем данные запроса
      $this->success_files = $this->string_to_array($_POST['filePathSuccess']);
      $this->error_files = $this->string_to_array($_POST['filePathError']);
      $this->popup_files = $this->string_to_array($_POST['filePathPopup']);
      $this->form_ids = $this->string_to_array($_POST['formIds']);
      $this->popup_btns_id = $this->collect_popup_btns_id($this->form_ids);
      $this->to_email = $_POST['msgEmail'];
      $this->msg_subjects = $this->string_to_array($_POST['msgSubject']);
      $this->visualFormCss = $_POST['visualFormCss'];

      // Считаем кол-во форм
      $this->forms_num = count($this->form_ids);

      // Сохраняем настройки
      $config_fields = array(
        'success_files'   => $this->success_files,
        'error_files'     => $this->error_files,
        'popup_files'     => $this->popup_files,
        'form_ids'        => $this->form_ids,
        'to_email'        => $this->to_email,
        'msg_subjects'    => $this->msg_subjects,
        'visual_form_css' => $this->visualFormCss
      );
      $this->save_config('form-normalizer/config.php', $config_fields);

      // Создаем папку проекта
      $this->create_dir('../wb-helper/form-normalizer');

      // Добавляем дополнительный fix-файл
      $this->create_dir('../wb-helper/css');
      file_put_contents('../wb-helper/css/style.fix.css', '', FILE_APPEND);

      // Собираем файлы
      $this->website_pages = $this->collect_files();

      // Fix input radio and checkbox position
      $this->fix_input_position($this->website_pages);
      
      // Добавляем файлы исходного шаблона
      $this->recursive_copy('form-normalizer/template/copy', '../wb-helper/form-normalizer/');
      
      // Генерируем файл output_msg.php
      $output_file = '../wb-helper/form-normalizer/output_msg.php';
      $fp = fopen($output_file, 'wb');
      if ($this->success_files) {
        $this->add_output_msg($fp, $this->form_ids, $this->success_files, 'success');
      }
      if ($this->error_files) {
        $this->add_output_msg($fp, $this->form_ids, $this->error_files, 'error');
      }
      fclose($fp);

      // Генерируем файл popup_forms.php
      $popup_forms_file = '../wb-helper/form-normalizer/popup_forms.php';
      $fp = fopen($popup_forms_file, 'wb');
      if ($this->popup_files) {
        $this->add_popup_forms($fp, $this->form_ids, $this->popup_files, 'popup-form');
      }
      fclose($fp);

      // Собираем стили формы и сообщений в один css-файл
      $output_css_file = '../wb-helper/form-normalizer/forms_and_msgs.css';
      $css_files = [$this->success_files, $this->error_files, $this->popup_files];
      $this->generate_css_file($output_css_file, $css_files);
      
      // Генерируем файл common.js
      $common_file = '../wb-helper/form-normalizer/common.js';
      $fp = fopen($common_file, 'wb');
      $content = file_get_contents('form-normalizer/template/generate/common.js');
      
      $arr = '';
      $val = '';

      for ($i=0; $i < $this->forms_num; $i++) {
        $val = $this->form_ids[$i];
        $arr .= "'" . $val . "': '". $this->msg_subjects[$i] ."',";
      }

      $search = [
        "//##var msgSubject = {};##",
        "//##formFields.to_email = '';##",
        "//##var popupFormBtns = [];##"
      ];
      $replace = [
        "var msgSubject = {".$arr."};",
        "formFields.to_email = '".$this->to_email."';",
        "var popupFormBtns = '".$this->popup_btns_id."';"
      ];
      $content = str_replace($search, $replace, $content);
      fwrite($fp, mb_convert_encoding($content, 'UTF-8'));
      fclose($fp);

      // Fix #container wrapper
      // $this->fix_container($this->website_pages);

      // json messages
      $this->json['recursive_copy'] = $this->files_copied;
      $this->json['success_files'] = $this->success_files;
      $this->json['error_files'] = $this->error_files;
      $this->json['popup_files'] = $this->popup_files;
      $this->json['form_ids'] = $this->form_ids;
      $this->json['popup_btns_id'] = $this->popup_btns_id;
      $this->json['to_email'] = $this->to_email;
      $this->json['msg_subjects'] = $this->msg_subjects;
      $this->json['website_pages'] = $this->website_pages;
      return $this->json;
    }

    protected function fix_container($files) {
      foreach ($files as $file) {
        //$file = '/home/a/adssslhj/msk-lg.ru/public_html/videonabludenie.php';
        // $file = '/home/a/adssslhj/msk-lg.ru/public_html/forms/cbf_ok.php';
        $html = file_get_html($file);

        $c = $html->find('#container', 0);

        if ($c) {
          $c_i = $c->innertext;

          // unwrap #container
          $html->find('#container', 0)->outertext = $c_i;
          
          $body = $html->find('body', 0);
          $body_i = $body->innertext;

          $wpb1 = $body->find('#output-msg', 0)->outertext;
          $wpb2 = $body->find('#popup-forms', 0)->outertext;
          $wpb3 = $body->find('#preload-imgs', 0)->outertext;
          $wpb4 = $body->find('#dialog-box-overlay', 0)->outertext;

          // echo "----------------------------";
          // echo $wpb1.$wpb2.$wpb3.$wpb4;
          // echo "----------------------------";

          // remove special blocks
          // foreach($html->find('#output-msg, #popup-forms, #preload-imgs, #dialog-box-overlay') as $block) {
          //   $block->outertext = '';
          // }

          // remove params 
          
          // wrap body innertext by #container and append special blocks
          //$html->find('body', 0)->innertext = '<div id="container">'.$body_i.'</div>'.$wpb1.$wpb2.$wpb3.$wpb4;
          $html->find('body', 0)->innertext = '<div id="container">'.$body_i.'</div>';

          $html->save($file);
        }
        $html->clear(); 
        unset($html);
      }
    }

    protected function collect_popup_btns_id($form_ids) {
      $result = '';

      foreach ($form_ids as $id) {
        if (strpos($id, ':') !== false) {
          $result .= substr($id, strpos($id, ':') + 1) . ', ';
        }
      }
      $result = substr($result, 0, -2);

      return $result;
    }

    protected function fix_input_position(array $files) {
      $regex = '/(type="radio|checkbox")(.*?)(style=".*?")(.*?)(<label)/';

      foreach ($files as $file) {
        $content = file_get_contents($file);
        $content = preg_replace_callback(
          $regex, 
          function ($m) {
            $m[3] = 'style="top:0;left:0;z-index:1;position:absolute;"';
            return $m[1].$m[2].$m[3].$m[4].$m[5];
          },
          $content
        );
        file_put_contents($file, $content);
      }
    }

    protected function collect_files() {
      $p = str_replace('wb-helper-app/form-normalizer', '', realpath(dirname(__FILE__)));
      $dir_iterator = new RecursiveDirectoryIterator($p);
      $iterator = new RecursiveIteratorIterator($dir_iterator, RecursiveIteratorIterator::SELF_FIRST);

      $files = array();
      foreach ($iterator as $path) {
        if ($path->isFile()) {
          $file = pathinfo($path);

          $i_dots = '..';
          $i_path = $file['dirname'].'/'.$file['basename'];
          $i_result = str_replace($p, '../', $file['dirname'].'/'.$file['basename']);
          $ignore_item = in_array($i_result, $this->popup_files) || in_array($i_result, $this->success_files);

          $name = $file['basename']; $ext = isset($file['extension']) ? $file['extension'] : false; $dir = $file['dirname'];

          if (($ext == 'php' || $ext == 'html' || $ext == 'htm') && strpos($dir, 'wb-helper') == false && !$ignore_item) {
            $files[] = $dir.'/'.$name;
          }
        }
      }
      
      return $files;
    }

    protected function add_output_msg($fopen, array $form_ids, array $msg_paths, $msg_type) {
      $c = count($form_ids);
      $style = '';
      //$regex_content = "/<body.*\/body>/s";
      $regex_content = '/<div id=\"container\">(.*?)<div id=\"output-msg\"/si';
      $regex_last_div = '~<\/div>(?!.*<\/div>)~s';
      $regex_img_src = '/<img src=\"/';

      for ($i=0; $i < $c; $i++) {
        $msg = '';

        // If popup form - break. After ":" name of id btn for popup form 
        if (strpos($form_ids[$i], ':') !== false) {continue;}

        $content = file_get_contents($msg_paths[$i]);

        // Replace all img src
        $msg_path = str_replace('../', '', $msg_paths[$i]);
        $msg_folder = substr($msg_path, 0, strrpos($msg_path, '/') + 1);
        $content = preg_replace($regex_img_src, '<img src="'.$msg_folder, $content);

        // Get page style width and height
        $style = preg_match("'<div id=\"wb-params\" style=\"display:none;\">(.*?)</div>'si", $content, $style_match);
        if (!empty($style)) {
          $style = preg_replace('/\s+/', '', $style_match[1]);
        }
        // Remove params
        $content = preg_replace("'<div id=\"wb-params\" style=\"display:none;\">(.*?)</div>'si", '', $content);

        // get content
        preg_match($regex_content, $content, $match);
        $msg = !empty($match) ? $match[1] : $content;
        $msg = preg_replace($regex_last_div, '', $msg);

        $html  = '<!-- START <<<<< --><div class="dialog-box '.$form_ids[$i].' '.$msg_type.'" style="'.$style.'">';
        $html .= '  <div class="result-msg">';
        $html .= '    <span class="dialog-box__close"></span>';
        $html .= '    <div class="success-msg">';
        $html .=        $msg;
        $html .= '    </div>';
        $html .= '  </div>';
        $html .= '</div><!-- END >>>>> -->';

        fwrite($fopen, $html);
      }
    }

    protected function add_popup_forms($fopen, array $form_ids, array $forms_path, $css_class) {
      $btn_id = '';
      $success_msg = '';
      $error_msg = '';
      $style = '';
      $c = count( explode(',', $this->popup_btns_id) );
      $regex_content = '/<div id=\"container\">(.*?)<div id=\"output-msg\"/si';
      $regex_body = '/<body.*?>(.*?)<\/body>/si';
      $regex_last_div = '~<\/div>(?!.*<\/div>)~s';
      $regex_img_src = '/<img src=\"/';

      for ($i=0; $i < $c; $i++) {
        $form_path = str_replace('../', '', $forms_path[$i]);
        $form_folder = substr($form_path, 0, strrpos($form_path, '/') + 1);

        // After ":" name of id btn for popup form 
        if (strpos($form_ids[$i], ':') !== false) {
          $btn_id = substr($form_ids[$i], strpos($form_ids[$i], ':') + 1);
        } else {continue;}

        if (isset($this->success_files[$i])) {
          $success_msg = file_get_contents($this->success_files[$i]);
          $success_msg = preg_replace($regex_img_src, '<img src="'.$form_folder, $success_msg);
          preg_match($regex_content, $success_msg, $match);
          $success_msg = !empty($match) ? $match[1] : $success_msg;
          $success_msg = preg_replace($regex_last_div, '', $success_msg);
        }

        if (isset($this->error_files[$i])) {
          $error_msg = file_get_contents($this->error_files[$i]);
          $error_msg = preg_replace($regex_img_src, '<img src="'.$form_folder, $error_msg);
          preg_match($regex_content, $error_msg, $match);
          $error_msg = !empty($match) ? $match[1] : $error_msg;
          $error_msg = preg_replace($regex_last_div, '', $error_msg);
        }

        $content = file_get_contents($forms_path[$i]);
        
        // Get page style width and height
        $style = preg_match("'<div id=\"wb-params\" style=\"display:none;\">(.*?)</div>'si", $content, $style_match);
        if (!empty($style)) {
          $style = preg_replace('/\s+/', '', $style_match[1]);
        }
        // Remove params
        $content = preg_replace("'<div id=\"wb-params\" style=\"display:none;\">(.*?)</div>'si", '', $content);

        // Replace all img src
        $content = preg_replace($regex_img_src, '<img src="'.$form_folder, $content);

        // Get body content
        $content = preg_match($regex_body, $content, $body);
        $content = !empty($body) ? $body[1] : $body;
        
        // Get content if exists
        preg_match($regex_content, $content, $match);
        $content = !empty($match) ? $match[1] : $content;
        $content = preg_replace($regex_last_div, '', $content);

        $html  = '<!-- START <<<<< --><div class="dialog-box '.$btn_id.' '.$css_class.'" style="'.$style.'">';
        $html .= '  <div class="popup-form-wrapper active">';
        $html .= '    <span class="dialog-box__close"></span>';
        $html .=      $content;
        $html .= '  </div>';
        $html .= '  <div class="result-msg">';
        $html .= '    <span class="dialog-box__close"></span>';
        $html .= '    <div class="success-msg">' . $success_msg . '</div>';
        $html .= '    <div class="error-msg">' . $error_msg . '</div>';
        $html .= '  </div>';
        $html .= '</div><!-- END >>>>> -->';

        fwrite($fopen, $html);
      }
    }

    protected function generate_css_file($output_file, $input_arrays) {
      $regex_container = '/div#container(.*?)}/si';
      $regex_body = '/body(.*?)}/si';
      $regex_bg_img = '/background-image: url\(/';
      $arr = array();

      $fp = fopen($output_file, 'wb');

      $k = 0;
      foreach ($input_arrays as $item) {
        $k++;

        if (is_array($item)) {

          // $arr = array_unique($item);
          $arr = $item;
          foreach ($arr as $path) {
            $folder = substr($path, 0, strrpos($path, '/') + 1);
            $folder = '../'.$folder;

            $css_file = substr($path, 0, strrpos($path, '.') + 1).'css';

            if (file_exists($css_file)) {
              $content = file_get_contents($css_file);
              $content = preg_replace($regex_body, '', $content);
              $content = preg_replace($regex_bg_img, 'background-image: url('.$folder, $content);

              if ($k == 1) { // 1 array is success_files
                $this->height_arr = array();
                $this->height_no_media_arr = array();

                //$this->debug('form id', $this->form_ids[$this->success_files_c]);

                // проходим каждый @media
                $content = preg_replace_callback(
                  '/(@media[^{]+\{)([\s\S]+?})(\s*})/i', 
                  function ($m) {
                    // Парсим высоту элементов
                    $result = preg_replace_callback(
                      '/([\s\S]+?)(top: )(.*)(px;)([\s\S]+?)(height: )(.*)(px;)([\s\S]+?)/i',
                      function($l) {
                        $this->height_arr[] = $l[3]+$l[7];
                        return $l[1].$l[2].($l[3] - 15).$l[4].$l[5].$l[6].$l[7].$l[8];
                      },
                      $m[2]
                    );

                    // Заменяем #content классом попап окна, смещаем влево, добавляем высоту
                    $result = preg_replace_callback(
                      '/(div#container)([^}]+)(width: )(.*?)(px;)/i',
                      function($j) {
                        $height = max($this->height_arr) + 15;
                        $id = explode(':', $this->form_ids[$this->success_files_c]);
                        $id = isset($id[1]) ? $id[1] : $id[0];

                        $j[1] = '.dialog-box.'.$id.' .success-msg'; // form id
                        $j[4] = floatval($j[4] - 80);
                        $j[5] = 'px !important;';
                        $j[6] = 'height: '.($height - 30).'px;';
                        
                        $success = '.dialog-box.'.$id.'.dialog-box--active.success .result-msg {height: '.($height - 30).'px !important; width: '.$j[4].'px !important;}';

                        return $success.$j[1].$j[2].$j[3].$j[4].$j[5]."\n\t".$j[6]."\n";
                      },
                      $result
                    );

                    $result = preg_replace_callback(
                      '/([_a-zA-Z0-9->\. ]+|)(#-?)([_a-zA-Z]+[_a-zA-Z0-9-]*(?=[^}]*\{))/i',
                      function($m) {
                        $id = explode(':', $this->form_ids[$this->success_files_c]);
                        $id = isset($id[1]) ? $id[1] : $id[0];

                        return '.dialog-box.'.$id.' .success-msg '.trim($m[1]).$m[2].$m[3];
                      },
                      $result
                    );

                    // Уменьшаем значение left на 25px
                    $result = preg_replace_callback(
                      '/([\s\S]+?)(left: )(.*)(px;)([\s\S]+?)/i',
                      function($l) {
                        $l[3] = $l[3]-25;
                        return $l[1].$l[2].$l[3].$l[4];
                      },
                      $result
                    );

                    return $m[1].$result.$m[3];
                  }, 
                  $content
                );

                // проходим все до @media или до конца строки если @media нет
                $content = preg_replace_callback(
                  '/(^)([\s\S]+?)(@media|$)/i', 
                  function ($m) {

                    // Парсим высоту элементов
                    $result1 = preg_replace_callback(
                      '/([\s\S]+?)(top: )(.*)(px;)([\s\S]+?)(height: )(.*)(px;)([\s\S]+?)/i',
                      function($l) {
                        $this->height_arr[] = $l[3]+$l[7];
                        return $l[1].$l[2].($l[3] - 15).$l[4].$l[5].$l[6].$l[7].$l[8];
                      },
                      $m[2]
                    );

                    // Заменяем #content классом попап окна, смещаем влево, добавляем высоту
                    $result2 = preg_replace_callback(
                      '/(div#container)([^}]+)(width: )(.*?)(px;)/i',
                      function($j) {
                        $height = !empty($this->height_arr) ? (max($this->height_arr) + 15) : 15;
                        $id = explode(':', $this->form_ids[$this->success_files_c]);
                        $id = isset($id[1]) ? $id[1] : $id[0];

                        $j[1] = '.dialog-box.'.$id.' .success-msg'; // form id
                        $j[4] = floatval($j[4] - 80);
                        $j[5] = 'px;';
                        $j[6] = 'height: '.($height - 30).'px;';
                        
                        $success = '.dialog-box.'.$id.'.dialog-box--active.success .result-msg {height: '.($height - 30).'px !important; width: '.$j[4].'px !important;}';

                        return $success.$j[1].$j[2].$j[3].$j[4].$j[5]."\n\t".$j[6]."\n";
                      },
                      $result1
                    );

                    $result3 = preg_replace_callback(
                      '/([_a-zA-Z0-9->\. ]+|)(#-?)([_a-zA-Z]+[_a-zA-Z0-9-]*(?=[^}]*\{))/i',
                      function($m) {
                        $id = explode(':', $this->form_ids[$this->success_files_c]);
                        $id = isset($id[1]) ? $id[1] : $id[0];

                        return '.dialog-box.'.$id.' .success-msg '.trim($m[1]).$m[2].$m[3];
                      },
                      $result2
                    );

                    // Уменьшаем значение left на 25px
                    $result4 = preg_replace_callback(
                      '/([\s\S]+?)(left: )(.*)(px;)([\s\S]+?)/i',
                      function($l) {
                        $l[3] = $l[3]-25;
                        return $l[1].$l[2].$l[3].$l[4];
                      },
                      $result3
                    );

                    return $m[1].$result4.$m[3];
                  }, 
                  $content
                );

                $this->success_files_c++;
              }

              if ($k == 3) { // 3 array is popup_files
                $this->height_arr = array();
                $this->height_no_media_arr = array();

                // проходим каждый @media
                $content = preg_replace_callback(
                  '/(@media[^{]+\{)([\s\S]+?})(\s*})/i', 
                  function ($m) {
                    // Парсим высоту элементов
                    $result = preg_replace_callback(
                      '/([\s\S]+?)(top: )(.*)(px;)([\s\S]+?)(height: )(.*)(px;)([\s\S]+?)/i',
                      function($l) {
                        $this->height_arr[] = $l[3]+$l[7];
                        $this->modal_heights['popup_files'][$this->count_popup_media][] = $l[3]+$l[7];
                        return $l[1].$l[2].$l[3].$l[4].$l[5].$l[6].$l[7].$l[8];
                      },
                      $m[2]
                    );

                    // Заменяем #content классом попап окна, смещаем влево, добавляем высоту
                    $result = preg_replace_callback(
                      '/(div#container)([^}]+)(width: )(.*?)(px;)/i',
                      function($j) {
                        $height = max($this->height_arr) + 15;
                        $id = explode(':', $this->form_ids[$this->popup_files_c]);
                        $id = isset($id[1]) ? $id[1] : $id[0];

                        $j[1] = '.dialog-box.'.$id.' .popup-form-wrapper'; // form id
                        $j[4] = floatval($j[4] - 50);
                        $j[5] = 'px !important;';
                        $j[6] = 'height: '.$height.'px !important;';

                        return $j[1].$j[2].$j[3].$j[4].$j[5]."\n\t".$j[6]."\n";
                      },
                      $result
                    );

                    // Уменьшаем значение left на 25px
                    $result = preg_replace_callback(
                      '/([\s\S]+?)(left: )(.*)(px;)([\s\S]+?)/i',
                      function($l) {
                        $l[3] = $l[3]-25;
                        return $l[1].$l[2].$l[3].$l[4];
                      },
                      $result
                    );

                    return $m[1].$result.$m[3];
                  }, 
                  $content
                );

                // проходим все до @media или до конца строки если @media нет
                $content = preg_replace_callback(
                  '/(^)([\s\S]+?)(@media|$)/i', 
                  function ($m) {
                    // Парсим высоту элементов
                    $result = preg_replace_callback(
                      '/([\s\S]+?)(top: )(.*)(px;)([\s\S]+?)(height: )(.*)(px;)([\s\S]+?)/i',
                      function($l) {
                        $this->height_no_media_arr[] = $l[3]+$l[7];
                        return $l[1].$l[2].$l[3].$l[4].$l[5].$l[6].$l[7].$l[8];
                      },
                      $m[2]
                    );

                    // Заменяем #content классом попап окна, смещаем влево, добавляем высоту
                    $result = preg_replace_callback(
                      '/(div#container)([^}]+)(width: )(.*?)(px;)/i',
                      function($j) {
                        $height = !empty($this->height_no_media_arr) ? (max($this->height_no_media_arr) + 15) : 15;
                        $id = explode(':', $this->form_ids[$this->popup_files_c]);
                        $id = isset($id[1]) ? $id[1] : $id[0];

                        $j[1] = '.dialog-box.'.$id.' .popup-form-wrapper'; // form id
                        $j[4] = floatval($j[4] - 50);
                        $j[5] = 'px !important;';
                        $j[6] = 'height: '.$height.'px !important;';

                        return $j[1].$j[2].$j[3].$j[4].$j[5]."\n\t".$j[6]."\n";
                      },
                      $result
                    );

                    // Уменьшаем значение left на 25px
                    $result = preg_replace_callback(
                      '/([\s\S]+?)(left: )(.*)(px;)([\s\S]+?)/i',
                      function($l) {
                        $l[3] = $l[3]-25;
                        return $l[1].$l[2].$l[3].$l[4];
                      },
                      $result
                    );

                    return $m[1].$result.$m[3];
                  }, 
                  $content
                );

                $this->popup_files_c++;
              }

              // Удаляем оставшиеся #content
              //$content = preg_replace($regex_container, '', $content);

              fwrite($fp, $content);
            }

          }

        }

      }

      fclose($fp);
    }

    protected function create_dir($dir_path) {
      if (!file_exists($dir_path)) {
        mkdir($dir_path, 0777, true);
        $this->json['create_dir'] = 'folder '+$dir_path+' created';
      } else {
        $this->json['create_dir'] = 'folder '+$dir_path+' exists';
      }
    }

    protected function recursive_copy($src, $dst) {
      $dir = @opendir($src);
      
      if (is_bool($dir)) { return false; }

      @mkdir($dst); 
      while(false !== ( $file = readdir($dir)) ) { 
        if (( $file != '.' ) && ( $file != '..' )) { 
          if ( is_dir($src . '/' . $file) ) { 
            $this->recursive_copy($src . '/' . $file, $dst . '/' . $file); 
          }
          else {
            copy($src . '/' . $file, $dst . '/' . $file);
            $this->files_copied++;
          }
        } 
      } 
      closedir($dir);
    }
    
    protected function string_to_array($string, $replace = '/\r|\n/') {
      $result = false;
      if ($string) {
        $result = preg_replace($replace, ',', $string);
        $result = explode(',', $result);
      }
      return $result;
    }

    protected function save_config($filename, array $config) {
      $config = var_export($config, true);
      file_put_contents($filename, "<?php echo json_encode($config);");
    }

    protected function debug($title, $var, $clear = false) {
      $file = '/home/a/adssslj6/pandora-signalizaciya.ru/public_html/log.txt';

      $var = !is_array($var) ? $var : var_export($var, true);

      $result  = "\n--------------------\n";
      $result .= date("Y-m-d H:i:s:m")."\n".$title."\n".$var;

      if ($clear) { file_put_contents($file, ''); }

      file_put_contents($file, $result, FILE_APPEND);
    }

  }

?>