<?php 

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

// check if its an ajax request, exit if not
if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
  $output = json_encode(array(
    'type'=>'error', 
    'text' => 'Sorry Request must be Ajax POST'
  ));
  die($output);
}

$post = (array)$_POST;
$fields = array();

foreach ($post as $key => $value) {
  if (strpos($key, '##') !== false) {
    $new_key = str_replace('##', '', $key);
    $new_value = (array)json_decode(html_entity_decode($value));

    if (isset($new_value['value']) && is_object($new_value['value'])) {
      foreach ($new_value as $key2 => $value2) {
        $new_value[$key2] = (array)$value2;
      }
    }

    $fields[$new_key] = (array)$new_value;
  } 
}

$host        = $_SERVER['HTTP_HOST'];
$from_email  = 'noreply@'.$host;
$to_email    = isset($fields['to_email'][0]) ? $fields['to_email'][0] : false;
$subject     = isset($fields['subject'][0]) ? $fields['subject'][0] : false;
$boundary    = md5(uniqid(time()));
$file_status = '';
$eol         = "\n";
$attachment  = false;
$files       = isset($_FILES) ? $_FILES : false;

if (is_array($files)) {
  $attachment = reset($files);
}

$file_count  = $attachment ? count($attachment) : 0;

unset($fields['to_email']);
unset($fields['subject']);

$header  = 'From: '.$from_email.$eol;
$header .= 'Reply-To: '.$from_email.$eol;
$header .= 'MIME-Version: 1.0'.$eol;
$header .= 'Content-Type: multipart/mixed; boundary="'.$boundary.'"'.$eol;
$header .= 'X-Mailer: PHP v'.phpversion().$eol;

// create email message 
$message = $eol;

// foreach($fields as &$val) {
//   //echo "<pre>"; print_r($val); echo "</pre>";
//   if (is_array($val)) {
//     array_values($val);
//   }
// }

//echo "<pre>"; print_r($fields); echo "</pre>";

foreach ($fields as $key => $value) {
  $type = is_array($fields[$key]['type']) ? $fields[$key]['type'][0] : $fields[$key]['type'];
  if ($type == 'radio') {
    // $fields = array_values($fields[$key]);
    //$val = $fields[$key]['value'][0];
    
    //$val = isset($fields[$key]['value']['checked']) ? $fields[$key]['value']['checked'] : false;
    //$title = $fields[$key]['title'][0];
    $title = implode('', $fields[$key]['title']);
    $val = implode('', $fields[$key]['value']);

  } else {
    $title = $fields[$key]['title'];
    $val = $fields[$key]['value'];
  }

  if ($key == 'file' && is_array($fields[$key]['type'])) {
    //$message .= $fields[$key]['title'][0].':<br>';
  } elseif (!empty($val)) {
    $message .= $title.': <br><b>'.$val.'</b><br><br>';
  }
}
$message .= '<br>';

$body  = 'This is a multi-part message in MIME format.'.$eol.$eol;
$body .= '--'.$boundary.$eol;
$body .= 'Content-Type: text/html; charset=UTF-8'.$eol;
$body .= 'Content-Transfer-Encoding: 8bit'.$eol;
$body .= $eol.stripslashes($message).$eol;


if ($attachment) {
  if($attachment['error'] > 0) {
    $mymsg = array( 
      1 => "The uploaded file exceeds the upload_max_filesize directive in php.ini", 
      2 => "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form", 
      3 => "The uploaded file was only partially uploaded", 
      4 => "No file was uploaded", 
      6 => "Missing a temporary folder"
    );
    $file_status = $mymsg[$attachment['error']];
  } else {
    $body .= '--'.$boundary.$eol;
    $body .= 'Content-Type: '.$attachment['type'].'; name='.$attachment['name'].$eol;
    $body .= 'Content-Transfer-Encoding: base64'.$eol;
    $body .= 'Content-Disposition: attachment; filename='.$attachment['name'].$eol;
    $body .= $eol.chunk_split(base64_encode(file_get_contents($attachment['tmp_name']))).$eol;
    $body .= '--'.$boundary.'--'.$eol;
  }
}


//proceed with PHP email.
$send_mail = mail($to_email, $subject, $body, $header);

if(!$send_mail) {
  $output = json_encode(array(
    'type'=>'error',
    'text' => 'Ошибка отправки! Please check your PHP mail configuration.'
  ));
  die($output);
} else {
  $output = json_encode(array(
    'type'=>'message',
    'text' => 'Thanks! Ваше сообщение отправлено! '.$file_status,
  ));
  die($output);
}

?>