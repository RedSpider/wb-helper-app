jQuery(document).ready(function($) {
  
  var getCSS = function (prop, selector) {
    var $inspector;
    if (selector.indexOf('.') !== -1) {
      selector = selector.replace(/\./g, ' ');
      $inspector = $("<div>").attr('style', 'display:none; max-height:initial !important; position: absolute !important;').addClass(selector);
    } else if (selector.indexOf('#') !== -1) {
      selector = selector.replace('#', '');
      $inspector = $("<div>").attr('style', 'display:none; max-height:initial !important; position: absolute !important;').attr('id', selector);
    } else { // class without .
      $inspector = $("<div>").attr('style', 'display:none; max-height:initial !important; position: absolute !important;').addClass(selector);
    }

    $("body").append($inspector); // add to DOM, in order to read the CSS property
    try {
      return $inspector.css(prop);
    } finally {
      $inspector.remove(); // and remove from DOM
    }
  };

  var fElements = [];

  $("html *").each(function(indx, element){
    if ($(element).css('position') == 'fixed') {
      var curPadding = $(element).css('padding');
      fElements.push([$(element), curPadding]);
    }
  });

  (function(){
    //##var msgSubject = {};##

    $.each(msgSubject, function(id, subject) {
      var form = '',
          dialogClass = '',
          btnId = '';

      // if popup form
      if (/:/i.test(id)) {
        var arr = id.split(':');
        btnId = arr[1];
        form = $('.dialog-box.' + btnId + '.popup-form form');
        dialogClass = btnId;
      } else {
        form = $('#' + id);
        dialogClass = id;
      }

      form.attr('novalidate', 'true');

      form.submit(function(e) {
        var self = $(this),
            allFields = self.find('input:not(.form-control), textarea, select'),
            formFields = collectFields(allFields);
            proceed = isValid(formFields, form),
            formFields.subject = msgSubject[id];
            //##formFields.to_email = '';##
        
        //console.log('formFields');
        //console.log(formFields);
        
        var formData = new FormData(this);

        // По-другому никак? "##" дабы различать поля
        for (var key in formFields) {
          formData.append(key + '##', JSON.stringify(formFields[key]));
        }

        if (proceed) {
          $.ajax({
            url: 'wb-helper/form-normalizer/contact_me.php',
            type: 'post',
            data: formData,
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
          }).done(function(json) {
            console.log('success! Show message in modal box for form with id '+id);

            // check if popup form
            if (form.parents('.popup-form-wrapper').length) {
              console.log('check if popup form 1');
              form.parents('.popup-form-wrapper').toggleClass('active');
              form.parents('.popup-form').find('.success-msg').toggleClass('active');
              form.parents('.popup-form').find('.result-msg ').toggleClass('active');
              form.parents('.dialog-box').toggleClass('success');

              //$('html, body').animate({scrollTop : 0}, 300);
            } else {
              console.log('<<<<');
              console.log(dialogClass);
              console.log('>>>>');
              dialogBox('.dialog-box.'+dialogClass+'.success');
              $('.dialog-box.'+dialogClass+'.success').find('.success-msg').toggleClass('active');
              $('.dialog-box.'+dialogClass+'.success').find('.result-msg ').toggleClass('active');
            }

            $.each(allFields, function(elem, val) {
              var self = $(val),
              type = self.attr('type') || 'select';

              switch(type) {
                case 'select':
                  self.find('option:selected').prop('selected', false);
                break;
                case 'checkbox':
                  self.prop('checked', false);
                break;
                case 'radio':
                  self.prop('checked', false);
                break;
                case 'submit':
                break;
                default:
                  self.val('');
                break;
              }
            });
          })
          .fail(function() {
            console.log('--------------------------------');
            console.log(e);
            console.log('Ошибка, код: '+e.status);
            console.log('Status: %cerror', 'color: red');
            console.log('--------------------------------');
            //dialogBox('.dialog-box.'+dialogClass+'.error');

          })
          .always(function() {});
        }

        return false;
      });
    }); 

  })();

  // Replace attributes
  (function(){
    var allFields = $('form').find('input, textarea, select');
    $.each(allFields, function(id, elem) {
      var self = $(elem);
      var title = self.attr('title');
      var name = self.attr('name');
      var nameArr = [];

      if (typeof name !== typeof undefined && name !== false) {
        nameArr = name.split('__');
        self.attr('name', nameArr[0]);
        validate = (nameArr.length >= 2) ? nameArr[1] : '';
        ext      = (nameArr.length >= 3) ? nameArr[2] : '';
        maxSize  = (nameArr.length >= 4) ? nameArr[3] : '';
      }

      if (typeof ext !== typeof undefined && ext !== false) {
        self.attr('data-ext', ext);
      }

      if (typeof maxSize !== typeof undefined && maxSize !== false) {
        self.attr('data-maxsize', maxSize);
      }

      if (typeof validate !== typeof undefined && validate !== false) {
        self.attr('data-validate', validate);
      }

      if (typeof title !== typeof undefined && title !== false) {
        self.removeAttr('title');
        self.attr('data-title', title);
      }
    });

    $(document).click(function() {
      $('.tooltip-error').removeClass('active');
      $('input, textarea').removeClass('error');
    });
  })();

  // Click on btns for popup forms
  (function(){
    //##var popupFormBtns = [];##
    var btnIds = '';
    
    if (popupFormBtns.length) {
      btnIds = popupFormBtns.split(', ');

      $.each(btnIds, function(id, elem) {
        var btn = $('#'+elem);
  
        btn.click(function(e) {
          var self = $(this);
          var btnId = self.attr('id');
          var boxClass = '.dialog-box.'+btnId+'.popup-form';

          $(boxClass).find('.popup-form-wrapper').attr('class', 'popup-form-wrapper active');
          $(boxClass).find('.result-msg > div').removeClass('active');

          dialogBox(boxClass, e);
        });
      });
    }
  })();

  // Youtube iframe lazy load
  (function() {
      var allYoutube = $('iframe[src*="youtube.com"]');

      for (var i = 0; i < allYoutube.length; i++) {
        var id = $(allYoutube[i]).attr('src').split('embed/')[1].split('?')[0];
        $(allYoutube[i]).after('<div class="youtube-video" data-embed="'+id+'" style="height:100%; width:100%;"><div class="play-button"></div></div>');
        $(allYoutube[i]).remove();
      }

      var youtube = $(".youtube-video");

      for (var i = 0; i < youtube.length; i++) {
        var source = "https://i.ytimg.com/vi_webp/"+youtube[i].dataset.embed+"/hqdefault.webp";
        var image = new Image();

        image.src = source;
        image.addEventListener("load", function() {
          var div = '<div style="background: url('+image.src+') no-repeat 50% 50%;width: 100%;height: 100%;background-size: cover;"></div>';
          $(youtube[i]).append(div);
        }(i));

        youtube[i].addEventListener( "click", function() {
          var iframe = document.createElement( "iframe" );
          iframe.setAttribute( "frameborder", "0" );
          iframe.setAttribute( "style", "width:100%; height:100%;" );
          iframe.setAttribute( "allowfullscreen", "" );
          iframe.setAttribute( "src", "https://www.youtube.com/embed/"+ this.dataset.embed +"?rel=0&showinfo=0&autoplay=1" );

          this.innerHTML = "";
          this.appendChild(iframe);
        });
      }
  })();

  function dialogBox(elemClass, event) {
    var dialogBox = $(elemClass.replace('.success', '')),
        //dialogTrigger = $('.dialog-box__trigger'),
        //dialogOverlay = $('#dialog-box-overlay'),
        //dialogOverlay = dialogBox.find(' > div'),
        dialogClose = $('.dialog-box__close'),
        dialogTitle = $('.dialog-box__title'),
        dialogContent = $('.dialog-box__content'),
        dialogAction = $('.dialog-box__action'),
        dWidth = $(document).width(),
        dHeight = $(document).height(),
        wHeight = $(window).height(),
        wScrollTop = $(window).scrollTop(),
        scrollBarW = getScrollbarWidth();

    // Open the dialog
    if (dialogBox.parent().attr('id') == 'output-msg') {
      dialogBox.addClass('success');
    }
    dialogBox.addClass('dialog-box--active');
    $('body').addClass('dialog-box-opened');

    // setCssStyle(fElements, 'padding-right', scrollBarW+'px', 'set');

    if (event) {event.stopPropagation();}

    // Close the dialog - click close button
    dialogClose.on('click ', function() {
      dialogBox.removeClass('dialog-box--active success');
      dialogBox.find('.success-msg').removeClass('active');
      dialogBox.find('.result-msg').removeClass('active');
      $('body').removeClass('dialog-box-opened');
    });

    // Close the dialog - press escape key // key#27
    $(document).keyup(function(e) {
      if (e.keyCode === 27) {
        dialogBox.removeClass('dialog-box--active success');
        dialogBox.find('.success-msg').removeClass('active');
        dialogBox.find('.result-msg').removeClass('active');
        $('body').removeClass('dialog-box-opened');
      }
    });

    // Close dialog - click overlay
    $(document).on('click touchstart', dialogBox, function(e) {
      if(e.target != dialogBox[0]) return;

      dialogBox.removeClass('dialog-box--active success');
      dialogBox.find('.success-msg').removeClass('active');
      dialogBox.find('.result-msg').removeClass('active');
      $('body').removeClass('dialog-box-opened');
    });
  }

  function setCssStyle(elements, cssParam, cssValue, type) {
    $.each(elements, function(index, el) {
      if ($(el)[0].attr('id') == 'mm-main-box') {return true;}
      if (type == 'set' && cssValue) {
        $(el)[0].css(cssParam, cssValue);
      } else {
        $(el)[0].css(cssParam, $(el)[1]);
      }
    });
  }

  function collectFields(fieldsArr) {
    var resultObj = {};

    //console.log('fieldsArr');
    //console.log(fieldsArr);
    // console.log('============== each field ====');

    $.each(fieldsArr, function(id, elem) {
      var self     = $(elem),
          tag      = self.prop('tagName').toLowerCase(),
          name     = self.attr('name'),
          title    = self.attr('data-title'),
          validate = self.attr('data-validate'),
          ext      = self.attr('data-ext'),
          maxSize  = self.attr('data-maxsize'),
          type     = '',
          value    = '',
          checked  = false;

      switch(tag) {
        case 'select':
          type = 'select';
          value = self.find(':selected').val();
          title = (value == '-') ? false : title;
        break;
        case 'checkbox':
          value = (self.prop('checked') === true) ? 'Да' : '-';
          if (validate != '*') {
            title = (value == '-') ? false : title;
          }
        break;
        case 'textarea':
          type = 'textarea';
          value = self.val().replace(/\r\n|\r|\n/g,"<br />");
        break;
        default:
          checked = self.prop('checked');
          type = self.attr('type');
          value = self.val();
        break;
      }

      $.each([title, value, validate, ext, maxSize, type], function(id, elem) {
        elem  = (typeof elem !== typeof undefined && elem !== false) ? elem : '';
      });

      if (title || type == 'file') {
        if (type == 'file') {
          title = 'File';
          f = self[0].files[0] ? self[0].files[0] : false;
          ext = ext.split(','); // array
          maxSize = maxSize.length ? maxSize : '50'; // in MB
          
          value = {
            'name':       f ? f.name : '',
            'size':       f ? f.size : '',
            'type':       f ? f.type : '',
            'maxSize':    maxSize,
            'extensions': ext
          };
        }
        if (type == 'radio') {
          var itemId = !checked ? id : 'checked';
          if (!(name in resultObj)) {
            resultObj[name] = {
              'title': title,
              'type': type,
              'validate': validate,
              'value': {}
            };
          }
          if (checked) {
            resultObj[name].value[itemId] = value;
          }
        } else {
          resultObj[name] = {
            'title': title,
            'type': type,
            'validate': validate,
            'value': value
          };
        }
      }
    });
    
    console.log('resultObj');
    console.log(resultObj);

    return resultObj;
  }

  function addErrorMsg(elem, msgText, rebuildMsg) {
    var elType = elem.attr('type'),
        rebuild = rebuildMsg || false,
        elId = '',
        eStyle = '',
        eHeight = '';
      //console.log('--------------');
      //console.log(elem.attr('id'));

    if (elType == 'file' && (elem.attr('id') === null || elem.attr('id') === undefined)) {
      elId = elem.parents('div.input-group').attr('id');
      eHeight = elem.parents('div.input-group').height();
      //eStyle = elem.parents('div.input-group').attr('style');
      eStyle = 'left:0;top:0;position:absolute;';
    } else {
      if((elType == 'radio' || elType == 'checkbox') && (elem.next('label').length)) {
        elem = elem.parent('div');
        elId = elem.attr('id');
        eHeight = $('#'+elId).height();
        eStyle = $('#'+elId).attr('style');
      } else {
        elId = elem.attr('id');
        eHeight = elem.height();
        eStyle = elem.attr('style');
      }
    }

    var elErr = $('.'+elId+'-error');
    var style = '';

    // collect style params
    if (eStyle) {
      style = [];
      style = eStyle.split(';');
      style.splice(-1, 1);
      eStyle = {};
      for (var i = style.length - 1; i >= 0; i--) {
        temp = style[i].split(':');
        temp[0] = $.trim(temp[0]);
        temp[1] = $.trim(temp[1]);
        if (temp[0] == 'top') {
          if (elType == 'file' && (elem.attr('id') === null || elem.attr('id') === undefined)) {
            temp[1]  = parseFloat(temp[1]) + eHeight + 2;
            temp[1] += 'px';
          } else {
            temp[1]  = parseFloat(temp[1]) + eHeight + 6;
            temp[1] += 'px';
          }
        }
        eStyle[temp[0]] = temp[0] + ':' + temp[1] + ';';
      }
      style = eStyle.left + eStyle.top + eStyle.position;
    } else {
      var height = parseFloat(getCSS('height', '#'+elem.attr('id')));
      var top = parseFloat(getCSS('top', '#'+elem.attr('id'))) + height + 3;
      var left = parseFloat(getCSS('left', '#'+elem.attr('id')));
      style = 'position:absolute; top:' + top + 'px; left: ' + left + 'px;';
    }
    
    
    var html  = '<div class="'+elId+'-error tooltip-error active" style="'+style+'">';
        html += '<div><span><i></i>'+msgText+'</span></div>';
        html += '</div>';

    elErr.remove();
    if (elType == 'file' && elem.attr('id') === null) {
      elem.parents('div.input-group').after(html);
    } else if (elType == 'radio'){
      elem.first().after(html);
    } else {
      elem.after(html);
    }
  }

  function isValid(obj, formId) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/gm,
        phoneReg = /^(\+?\d+)?\s*(\(\d+\))?([\s-]*([\d-]*))*$/gm,
        valid = true;

    console.log('function isValid');
    console.log(obj);

    for (var prop in obj) {
      var field = $(formId).find('[name="'+prop+'"]'),
          elem = obj[prop],
          type = elem.type,
          value = elem.value;
          console.log('value: ');
          console.log(value);
      var validate = elem.validate ? elem.validate : '',
          ext = (elem.value.extensions !== undefined) ? elem.value.extensions : '',
          maxSize = elem.value.maxSize ? elem.value.maxSize : '',
          isPhone = typeof(value) == 'string' ? value.search(phoneReg) !== -1 : false,
          isEmail = typeof(value) == 'string' ? value.search(emailReg) !== -1 : false,
          parts = '',
          elId = '';

      if (type == 'file' && field.attr('id') === null) {
        elId = field.parents('div.input-group').attr('id');
      } else {
        elId = field.attr('id');
      }

      var elErr = $('.'+elId+'-error');

      field.removeClass('error');
      elErr.removeClass('active');

      console.log(elem);

      // валидация если не пустое поле [!*] или [*!] (/\*!|!\*/g)
      if (validate.search(/\*!|!\*/g) != -1) {
        if (type == 'email') {
          if (!isEmail && value.length !== 0) {
            valid = false;
            console.log('Поле [' + prop + '] не прошло проверку');
            addErrorMsg(field, 'Неверный формат email');
            field.addClass('error');
          }
        } else if (type == 'tel') {
          if (!isPhone && value.length !== 0) {
            valid = false;
            console.log('Поле [' + prop + '] не прошло проверку');
            addErrorMsg(field, 'Неверный формат телефона');
            field.addClass('error');
          }
        } else if (type == 'file') {
          //console.log('File type');
          //console.log(value.name.length);

          if (!hasExtension(value.name, ext) && value.name.length !== 0) {
            valid = false;
            console.log('!hasExtension, not allowed');
            addErrorMsg(field, 'Недопустимый формат' );
          } else if (!validSize(value.size, maxSize) && value.name.length !== 0) {
            valid = false;
            console.log('!validSize, размер не должен быть больше чем '+maxSize);
            addErrorMsg(field, 'Файл должен быть менее ' + maxSize + ' MB' );
          }
        }

      // валидация [!] + не пустое поле
      } else if (validate.search(/!/g) != -1) {
        if (type == 'email' && (!isEmail || value.length === 0)) {
          valid = false;
          console.log('Поле [' + prop + '] не прошло проверку');
          addErrorMsg(field, 'Неверный формат email');
          field.addClass('error');

        } else if (type == 'tel' && (!isPhone || value.length === 0)) {
          valid = false;
          console.log('Поле [' + prop + '] не прошло проверку');
          addErrorMsg(field, 'Неверный формат phone');
          field.addClass('error');

        } else if (type == 'file') {
          console.log('File type');
          console.log(value.name.length);

          if (value.name.length === 0) {
            valid = false;
            console.log('File must be not empty!');
            addErrorMsg(field, 'Выбор файла обязателен' );
          } else if (!hasExtension(value.name, ext)) {
            valid = false;
            console.log('!hasExtension, not allowed');
            addErrorMsg(field, 'Недопустимый формат' );
          } else if (!validSize(value.size, maxSize)) {
            valid = false;
            console.log('!validSize, размер не должен быть больше чем '+maxSize);
            addErrorMsg(field, 'Файл должен быть менее ' + maxSize + ' MB' );
          }
        }

      //  не пустое поле [*]
      } else if (validate.search(/\*/g) != -1) {
        
        //console.log('value');
        //console.log(value);

        if (type == 'radio' && !('checked' in value) && value.length === 0) {
          valid = false;
          console.log('Значение поля radio [' + prop + '] не выбрано');
          addErrorMsg(field, 'Выбор обязателен');
        }
        if ((type == 'select' || type == 'checkbox') && value == '-') {
          valid = false;
          console.log('Значение поля select [' + prop + '] не выбрано');
          addErrorMsg(field, 'Выбор обязателен');
          field.addClass('error');
        }
        if ((type != 'select' && type != 'checkbox') && value.length < 2) {
          valid = false;
          console.log('Длина поля [' + prop + '] меньше 2 символов');
          addErrorMsg(field, 'Поле не должно быть пустым');
          field.addClass('error');
        }
      }

      if (!valid) {break;}
    }

    return valid;
  }

  function hasExtension(name, exts) {
    return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test(name);
  }

  function validSize(current, max) {
    // convert max (MB) to bytes
    max = max * 1024 * 1024;
    console.log('validSize current: '+current+'; max: '+max);
    return current <= max;
  }

  function getScrollbarWidth() {
    var outer = document.createElement("div");
    outer.style.visibility = "hidden";
    outer.style.width = "100px";
    outer.style.msOverflowStyle = "scrollbar"; // needed for WinJS apps

    document.body.appendChild(outer);

    var widthNoScroll = outer.offsetWidth;
    // force scrollbars
    outer.style.overflow = "scroll";

    // add innerdiv
    var inner = document.createElement("div");
    inner.style.width = "100%";
    outer.appendChild(inner);        

    var widthWithScroll = inner.offsetWidth;

    // remove divs
    outer.parentNode.removeChild(outer);

    return widthNoScroll - widthWithScroll;
  }

});