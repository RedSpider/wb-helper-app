<?php 

$autoGen = false;

if ($autoGen) {
  $c_t = collect_thumbs('images/', 'tn_');
  $o_t = get_thumbs_info('thumbs-info.json');

  $c_t_count = count($c_t);
  $o_t_count = count($o_t);

  if ($o_t_count) {
    foreach ($c_t as $c_key => $c_val) {
      foreach ($o_t as $o_key => $o_val) {
        $same_keys = $c_val['name'] == $o_key || false;
        $same_time = $c_val['created'] == $o_val || false;

        if ($same_keys && !$same_time) {
          unlink($c_val['path']);
          $c_t[$c_key]['created'] = create_thumb($c_val);
        }
      }
    }
  } else {
    for ($i=0; $i < $c_t_count; $i++) { 
      unlink($c_t[$i]['path']);
      $c_t[$i]['created'] = create_thumb($c_t[$i]);
    }
  }

  $thumbs_info = array();
  foreach ($c_t as $thumb) {
    $thumbs_info[$thumb['name']] = $thumb['created'];
  }

  save_config('thumbs-info.json', $thumbs_info, 'json');
}


function get_thumbs_info($filename) {
  if (file_exists($filename)) {
    $info = file_get_contents($filename);
    $info = json_decode($info, TRUE);
    return $info;
  }
}

function create_thumb(array $thumb_item) {
  $orig_path = str_replace($thumb_item['prefix'], '', $thumb_item['path']);
  $orig_name = str_replace($thumb_item['prefix'], '', $thumb_item['name']);

  $thumb_width = $thumb_item['width'];
  $thumb_height = $thumb_item['height'];

  $image_type = exif_imagetype($orig_path);

  switch ($image_type) {
    case '1':
      $image = imagecreatefromgif($orig_path);
      break;
    case '2':
      $image = imagecreatefromjpeg($orig_path);
      break;
    case '3':
      $image = imagecreatefrompng($orig_path);
      break;
  }

  $width = imagesx($image);
  $height = imagesy($image);
  
  $filename = $thumb_item['path']; //

  $original_aspect = $width / $height;
  $thumb_aspect = $thumb_width / $thumb_height;

  if ($original_aspect >= $thumb_aspect) {
     // If image is wider than thumbnail (in aspect ratio sense)
     $new_height = $thumb_height;
     $new_width = $width / ($height / $thumb_height);
  } else {
     // If the thumbnail is wider than the image
     $new_width = $thumb_width;
     $new_height = $height / ($width / $thumb_width);
  }

  $thumb = imagecreatetruecolor($thumb_width, $thumb_height);

  // Resize and crop
  imagecopyresampled($thumb,
                     $image,
                     0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
                     0 - ($new_height - $thumb_height) / 2, // Center the image vertically
                     0, 0,
                     $new_width, $new_height,
                     $width, $height);
  imagejpeg($thumb, $filename, 80);

  return filemtime($filename);
}

function save_config($filename, array $config, $filetype = 'php') {
  if ($filetype == 'php') {
    $config = var_export($config, true);
    file_put_contents($filename, "<?php echo json_encode($config);");
  } elseif ($filetype == 'json') {
    file_put_contents($filename, json_encode($config));
  }
}

function collect_thumbs($img_folder = '../images/', $thumb_prefix = 'tn_') {
  $thumbs_arr = array();

  foreach (glob($img_folder . '*') as $file) {
    $file_name = str_replace($img_folder, '', $file);
    $orig_file = str_replace($thumb_prefix, '', $file_name);

    if ($thumb_prefix == mb_substr($file_name, 0, 3) && file_exists($img_folder.$orig_file)) {
      $size = getimagesize($file);
      $thumbs_arr[] = array(
        'prefix' => $thumb_prefix,
        'path' => $file,
        'name' => $file_name,
        'created' => filemtime($file),
        'width' => $size[0],
        'height' => $size[1]
      );
    }
  }
  return $thumbs_arr;
}

?>