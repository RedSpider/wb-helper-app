<?php 
  class ThumbGalleryGenerator {

    private $json = array();
    private $thumbs_generated = 0;
    private $thumbs = array();

    public function start_helper() {
      $is_autogen = isset($_POST['isAutoGen']) ? $_POST['isAutoGen'] : 0;

      // Сохраняем настройки
      $config_fields = array(
        'is_autogen' => $is_autogen
      );
      $this->save_config('thumb-gallery-generator/config.php', $config_fields);

      // Создаем папку проекта
      $this->create_dir('../wb-helper/thumb-gallery-generator');

      // Ищем все миниатюры в images
      $this->thumbs = $this->collect_thumbs('../images/', 'tn_');

      $c = count($this->thumbs);
      for ($i=0; $i < $c; $i++) { 
        unlink($this->thumbs[$i]['path']);
        $this->thumbs[$i]['created'] = $this->create_thumb($this->thumbs[$i]);
      }

      $thumbs_info = array();
      foreach ($this->thumbs as $thumb) {
        $thumbs_info[$thumb['name']] = $thumb['created'];
      }

      $this->save_config('../wb-helper/thumb-gallery-generator/thumbs-info.json', $thumbs_info, 'json');

      $this->get_config('../wb-helper/thumb-gallery-generator/thumbs-info.json');

      // generate main.php
      $content = file_get_contents('thumb-gallery-generator/template/generate/main.php');
      $content = str_replace('$autoGen = false', '$autoGen = '.$is_autogen, $content);
      file_put_contents('../wb-helper/thumb-gallery-generator/main.php', $content);

      // json messages
      $this->json['thumbs_generated'] = $this->thumbs_generated;
      $this->json['thumbs_to_generate'] = $this->thumbs;
      return $this->json;
    }

    protected function get_config($filename) {
      if (file_exists($filename)) {
        $info = file_get_contents($filename);
        $info = json_decode($info, TRUE);
        return $info;
      }
    }

    protected function collect_thumbs($img_folder = '../images/', $thumb_prefix = 'tn_') {
      $thumbs_arr = array();

      foreach (glob($img_folder . '*') as $file) {
        $file_info = pathinfo($file);
        $file_name = str_replace($img_folder, '', $file);
        $name = str_replace($thumb_prefix, '', $file_name);
        $name = str_replace('.'.$file_info['extension'], '', $name);
        
        $names = [$name.'.jpg', $name.'.png', $name.'.gif', $name.'.jpeg'];
        $is_file_exists = (file_exists($img_folder.$names[0]) || file_exists($img_folder.$names[1]) || file_exists($img_folder.$names[2]) || file_exists($img_folder.$names[3])) ? '1' : '0';

        if ($thumb_prefix == mb_substr($file_name, 0, 3) && $is_file_exists) {
          $size = getimagesize($file);
          $thumbs_arr[] = array(
            'prefix' => $thumb_prefix,
            'path' => $file,
            'name' => $file_name,
            'created' => filemtime($file),
            'width' => $size[0],
            'height' => $size[1]
          );
        }
      }
      return $thumbs_arr;
    }

    protected function create_thumb(array $thumb_item) {
      $orig_path = str_replace($thumb_item['prefix'], '', $thumb_item['path']);
      $orig_name = str_replace($thumb_item['prefix'], '', $thumb_item['name']);
      $path_without_ext = explode(strrchr($orig_path, '.'), $orig_path)[0];

      if (file_exists($path_without_ext.'.png')) {
        $orig_path = $path_without_ext.'.png';
      } elseif(file_exists($path_without_ext.'.jpg')) {
        $orig_path = $path_without_ext.'.jpg';
      } elseif(file_exists($path_without_ext.'.jpeg')) {
        $orig_path = $path_without_ext.'.jpeg';
      } elseif(file_exists($path_without_ext.'.gif')) {
        $orig_path = $path_without_ext.'.gif';
      }

      $thumb_width = $thumb_item['width'];
      $thumb_height = $thumb_item['height'];

      $image_type = exif_imagetype($orig_path);

      switch ($image_type) {
        case '1':
          $image = imagecreatefromgif($orig_path);
          break;
        case '2':
          $image = imagecreatefromjpeg($orig_path);
          break;
        case '3':
          $image = imagecreatefrompng($orig_path);
          break;
      }

      $width = imagesx($image);
      $height = imagesy($image);
      
      $filename = $thumb_item['path']; //

      $original_aspect = $width / $height;
      $thumb_aspect = $thumb_width / $thumb_height;

      if ($original_aspect >= $thumb_aspect) {
         // If image is wider than thumbnail (in aspect ratio sense)
         $new_height = $thumb_height;
         $new_width = $width / ($height / $thumb_height);
      } else {
         // If the thumbnail is wider than the image
         $new_width = $thumb_width;
         $new_height = $height / ($width / $thumb_width);
      }

      $thumb = imagecreatetruecolor($thumb_width, $thumb_height);

      // Resize and crop
      imagecopyresampled($thumb,
                         $image,
                         0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
                         0 - ($new_height - $thumb_height) / 2, // Center the image vertically
                         0, 0,
                         $new_width, $new_height,
                         $width, $height);
      imagejpeg($thumb, $filename, 80);

      $this->thumbs_generated++;

      return filemtime($filename);
    }

    protected function create_dir($dir_path) {
      if (!file_exists($dir_path)) {
        mkdir($dir_path, 0777, true);
      }
    }

    protected function save_config($filename, array $config, $filetype = 'php') {
      if ($filetype == 'php') {
        $config = var_export($config, true);
        file_put_contents($filename, "<?php echo json_encode($config);");
      } elseif ($filetype == 'json') {
        file_put_contents($filename, json_encode($config));
      }
    }

  }
?>