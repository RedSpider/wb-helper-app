<?php 

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

if($_POST) {

  //check if its an ajax request, exit if not
  if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
    $output = json_encode(array(
      'type'=>'error', 
      'text' => 'Sorry Request must be Ajax POST'
    ));
    die($output);
  }
  
  $host        = gethostname();
  $to_email    = "##to_email##";
  $from_email  = 'noreply@'.$host;
  
  // Sanitize input data using PHP filter_var().
  $name        = getPostParam('name');
  $surname     = getPostParam('surname');
  $phone       = getPostParam('phone');
  $email       = getPostParam('email');
  $city        = getPostParam('city');
  $message     = getPostParam('message');
  
  $subject =   "##mail_subject##";

  // create email message 
  $message_body  = $subject ? ('<h3>'.$subject.'</h3>') : '<h3>Сообщение с сайта '.$host.'</h3>';
  $message_body .= $name ? ('Имя: <b>'.$name.'</b><br>') : '';
  $message_body .= $surname ? ('Фамилия: <b>'.$surname.'</b><br>') : '';
  $message_body .= $email ? ('E-mail: <b>'.$email.'</b><br>') : '';
  $message_body .= $phone ? ('Телефон: <b>'.$phone.'</b><br>') : '';
  $message_body .= $city ? ('Телефон: <b>'.$city.'</b><br>') : '';
  $message_body .= $message ? ('Сообщение: <b>'.$message.'</b><br>') : '';

  // proceed with PHP email.
  $headers  = 'Content-type: text/html; charset=utf-8 \r\n';
  $headers .= 'From: Info <'.$from_email.'>\r\n';
  
  $send_mail = mail($to_email, $subject, $message_body, $headers);
  
  if(!$send_mail) {
    $output = json_encode(array('type'=>'error', 'text' => 'Ошибка отправки! Please check your PHP mail configuration.'));
    die($output);
  } else {
    $output = json_encode(array('type'=>'message', 'text' => 'Спасибо! Ваше сообщение отправлено!'));
    die($output);
  }

  function getPostParam($post_var, $filter_type='FILTER_SANITIZE_STRING') {
    return ($_POST[$post_var]) ? filter_var($_POST[$post_var], $filter_type) : false;
  }
  
} ?>