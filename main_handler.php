<?php 
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

  require_once './form-normalizer/main.php';
  require_once './thumb-gallery-generator/main.php';
  require_once './gallery-combiner/main.php';

  $json = array();

  if (isset($_POST['type'])) {
    if ($_POST['type'] == 'form_normalizer') {
      $form_normalizer = new FormNormalizer();
      $json = $form_normalizer->start_helper();
    } elseif ($_POST['type'] == 'thumb_gallery_generator') {
      $thumb_generator = new ThumbGalleryGenerator();
      $json = $thumb_generator->start_helper();
    } elseif ($_POST['type'] == 'gallery_combiner') {
      $gallery_combiner = new GalleryCombiner();
      $json = $gallery_combiner->start_helper();
    }
  }

  echo json_encode($json);
?>